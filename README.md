# Desafio Api Transferencia

A API REST foi desenvolvida para atender os seguintes requisitos:

- Endpoint para cadastrar um cliente, com as seguintes informações: id (único), nome, número da conta (único) e saldo em conta;
- Endpoint para listar todos os clientes cadastrados;
- Endpoint para buscar um cliente pelo número da conta;
- Endpoint para realizar transferência entre 2 contas. A conta origem precisa ter saldo o suficiente para a realização da transferência e a transferência deve ser de no máximo R$ 1000,00 reais;
- Endpoint para buscar as transferências relacionadas à uma conta, por ordem de data decrescente. Lembre-se que transferências sem sucesso também devem armazenadas.

## Tecnologias

- [Spring Boot](https://spring.io/projects/spring-boot)
- [Spring MVC](https://docs.spring.io/spring-framework/reference/web/webmvc.html)
- [Spring Data JPA](https://spring.io/projects/spring-data-jpa)
- [SpringDoc OpenAPI 3](https://springdoc.org/#modules)

## Práticas adotadas

- SOLID, DRY, YAGNI, KISS
- API REST
- Consultas com Spring Data JPA
- Injeção de Dependências
- Tratamento de respostas de erro
- Geração automática do Swagger com a OpenAPI 3
- Testes unitários
- Testes integrados

## Como Executar

- Clonar repositório git
- Construir o projeto:
```
$ ./mvnw clean package
```
- Executar a aplicação:
```
$ java -jar target/desafioApiTransferencia-0.0.1-SNAPSHOT.jar
```

A API poderá ser acessada em [localhost:8080/api/usuario](http://localhost:8080/api/usuario) ou 
[localhost:8080/api/usuario/transferencia](http://localhost:8080/api/usuario/transferencia).
O Swagger poderá ser visualizado em [localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html)

## API Endpoints

Para fazer as requisições HTTP abaixo, foi utilizada a ferramenta [httpie](https://httpie.io):

- Criar Usuário
```
$ http POST :8080/api/usuario nome="Usuario 01" conta=10 saldo=1000

[
    {
        "conta": 1000,
        "id": 1,
        "nome": "Usuario 01",
        "saldo": 2000
    },
    {
        "conta": 2000,
        "id": 2,
        "nome": "Usuario 02",
        "saldo": 3000
    },
    {
        "conta": 3000,
        "id": 3,
        "nome": "Usuario 03",
        "saldo": 500
    },
    {
        "conta": 10,
        "id": 4,
        "nome": "Usuario 01",
        "saldo": 1000
    }
]

```

- Listar Usuários
```
$ http GET :8080/api/usuario

[
    {
        "conta": 1000,
        "id": 1,
        "nome": "Usuario 01",
        "saldo": 2000
    },
    {
        "conta": 2000,
        "id": 2,
        "nome": "Usuario 02",
        "saldo": 3000
    },
    {
        "conta": 3000,
        "id": 3,
        "nome": "Usuario 03",
        "saldo": 500
    },
    {
        "conta": 10,
        "id": 4,
        "nome": "Usuario 01",
        "saldo": 1000
  
```

- Listar Transferências
```
$ http GET :8080/api/usuario/transferencia/1000

[
    {
        "destino": 2000,
        "id": 2,
        "moment": "2024-03-05T22:34:01.611379Z",
        "motivo": "Transferência superior ao valor máximo: R$ 1000,00",
        "origem": 1000,
        "status": 0,
        "valor": 1500
    },
    {
        "destino": 2000,
        "id": 1,
        "moment": "2024-03-05T22:34:01.611371Z",
        "motivo": null,
        "origem": 1000,
        "status": 1,
        "valor": 500
    }
]
```

- Realizar Transferência
```
$ http POST :8080/api/usuario/transferencia origem=1000 destino=2000 valor=300

{
    "destino": 2000,
    "id": 7,
    "moment": "2024-03-05T22:44:20.308594635Z",
    "motivo": null,
    "origem": 1000,
    "status": 1,
    "valor": 300
}

```