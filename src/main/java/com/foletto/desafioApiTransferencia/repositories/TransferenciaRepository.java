package com.foletto.desafioApiTransferencia.repositories;

import com.foletto.desafioApiTransferencia.entities.Transferencia;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface TransferenciaRepository extends JpaRepository<Transferencia, Long> {

    @Query(nativeQuery = true, value = """
                    SELECT *
                    FROM tb_transferencia
                    WHERE origem = :contaId
                    ORDER BY moment DESC
            """)
    Optional<List<Transferencia>> listAllByUser(int contaId);
}
