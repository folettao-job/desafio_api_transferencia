package com.foletto.desafioApiTransferencia.repositories;

import com.foletto.desafioApiTransferencia.entities.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

    @Query(nativeQuery = true, value = """
                    SELECT *
                    FROM tb_usuarios
                    WHERE conta = :contaId
            """)
    Optional<Usuario> searchByConta(int contaId);
}
