package com.foletto.desafioApiTransferencia.services;

import com.foletto.desafioApiTransferencia.entities.Usuario;
import com.foletto.desafioApiTransferencia.repositories.UsuarioRepository;
import com.foletto.desafioApiTransferencia.services.exception.DatabaseException;
import com.foletto.desafioApiTransferencia.services.exception.ResourceNotFoundException;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class UsuarioService {
    private final UsuarioRepository usuarioRepository;

    /**
     * Construtor
     *
     * @param usuarioRepository
     */
    public UsuarioService(UsuarioRepository usuarioRepository) {
        this.usuarioRepository = usuarioRepository;
    }

    /**
     * Criação de usuário
     *
     * @param usuario: Usuario()
     * @return List<Usuario>
     */
    @Transactional(readOnly = true, propagation = Propagation.REQUIRED)
    public List<Usuario> create(Usuario usuario) {
        usuarioRepository.save(usuario);
        return this.listAll();
    }

    /**
     * Lista todos usuários cadstrados
     *
     * @return List<Usuario>)
     */
    @Transactional(readOnly = true)
    public List<Usuario> listAll() {
        return usuarioRepository.findAll();
    }

    /**
     * Lista usuários através do número da conta
     *
     * @param conta: int
     * @return Optional<Usuario>
     */
    @Transactional(readOnly = true)
    public Optional<Usuario> listByConta(int conta) {
        return usuarioRepository.searchByConta(conta);
    }

    /**
     * Realiza atualização dos dados do objeto Usuário
     *
     * @param entity: Usuario()
     * @param obj: Usuario()
     */
    private void updateData(Usuario entity, Usuario obj) {
        entity.setNome(obj.getNome());
        entity.setConta(obj.getConta());
        entity.setSaldo(obj.getSaldo());
    }

    /**
     * Realiza atualização do usuário
     *
     * @param id: Long
     * @param obj: Usuario()
     * @return Usuario()
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public Usuario update(Long id, Usuario obj) {
        try {
            Usuario entity = usuarioRepository.getReferenceById(id);
            updateData(entity, obj);
            return usuarioRepository.save(entity);
        } catch (EntityNotFoundException e) {
            throw new ResourceNotFoundException(e);
        }
    }

    /**
     * Realiza deleção do usuário
     *
     * @param id: Long
     * @return List<Usuario>
     */
    @Transactional(readOnly = true, propagation = Propagation.REQUIRED)
    public List<Usuario> delete(Long id) {
        try {
            usuarioRepository.deleteById(id);
            return this.listAll();
        } catch (EmptyResultDataAccessException e) {
            throw new ResourceNotFoundException(e);
        } catch (DataIntegrityViolationException e) {
            throw new DatabaseException(e.getMessage());
        }
    }

}
