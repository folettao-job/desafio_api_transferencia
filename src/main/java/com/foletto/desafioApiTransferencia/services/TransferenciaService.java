package com.foletto.desafioApiTransferencia.services;

import com.foletto.desafioApiTransferencia.entities.Transferencia;
import com.foletto.desafioApiTransferencia.repositories.TransferenciaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class TransferenciaService {
    private final TransferenciaRepository transferenciaRepository;
    private final UsuarioService usuarioService;

    /**
     * Construtor
     *
     * @param transferenciaRepository: TransferenciaRepository()
     * @param usuarioService: UsuarioService()
     */
    public TransferenciaService(TransferenciaRepository transferenciaRepository, UsuarioService usuarioService) {
        this.transferenciaRepository = transferenciaRepository;
        this.usuarioService = usuarioService;
    }

    /**
     * Realiza o processo de criação da transferência
     *
     * @param transferencia: Transferencia()
     * @return Transferencia()
     */
    @Transactional(readOnly = true, propagation = Propagation.REQUIRED)
    public Transferencia create(Transferencia transferencia) {
        if (transferencia.getValor() > 1000) {
            transferencia.setStatus(0);
            transferencia.setMotivo("Transferência superior ao valor máximo: R$ 1000,00");
            return transferenciaRepository.save(transferencia);
        }

        var origem = usuarioService.listByConta(transferencia.getOrigem());
        if (origem.isEmpty()) {
            transferencia.setStatus(0);
            transferencia.setMotivo("Conta de usuário origem inexistente");
            return transferenciaRepository.save(transferencia);
        }

        var destino = usuarioService.listByConta(transferencia.getDestino());
        if (destino.isEmpty()) {
            transferencia.setStatus(0);
            transferencia.setMotivo("Conta de usuário destino inexistente");
            return transferenciaRepository.save(transferencia);
        }
        if (origem.get().getConta() == destino.get().getConta()) {
            transferencia.setStatus(0);
            transferencia.setMotivo("As contas devem ser diferentes");
            return transferenciaRepository.save(transferencia);
        }

        if (origem.get().getSaldo() < transferencia.getValor()) {
            transferencia.setStatus(0);
            transferencia.setMotivo("Saldo insuficiente na conta de origem");
            return transferenciaRepository.save(transferencia);
        }

        origem.get().setSaldo(origem.get().getSaldo() - transferencia.getValor());
        destino.get().setSaldo(destino.get().getSaldo() + transferencia.getValor());

        usuarioService.update(origem.get().getId(), origem.get());
        usuarioService.update(destino.get().getId(), destino.get());

        transferencia.setStatus(1);
        return transferenciaRepository.save(transferencia);
    }

    /**
     * Lista todas transferências realizadas por uma determinada conta
     *
     * @param conta: int
     * @return Optional<List<Transferencia>>
     */
    @Transactional(readOnly = true)
    public Optional<List<Transferencia>> listAllByUser(int conta) {
        return transferenciaRepository.listAllByUser(conta);
    }
}
