package com.foletto.desafioApiTransferencia.services.exception;

public class ResourceNotFoundException extends RuntimeException {

    /**
     * Chamada para RuntimeException
     *
     * @param id: Object
     */
    public ResourceNotFoundException(Object id) { super("Resource not found. Id " + id); }
}
