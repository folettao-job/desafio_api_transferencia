package com.foletto.desafioApiTransferencia.services.exception;

public class DatabaseException extends RuntimeException {

    /**]
     * Chamada para RuntimeExcpetion
     *
     * @param msg: String
     */
    public DatabaseException(String msg) {
        super(msg);
    }
}
