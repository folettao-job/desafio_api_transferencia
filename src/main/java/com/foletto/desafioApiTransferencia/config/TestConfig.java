package com.foletto.desafioApiTransferencia.config;

import com.foletto.desafioApiTransferencia.entities.Transferencia;
import com.foletto.desafioApiTransferencia.entities.Usuario;
import com.foletto.desafioApiTransferencia.services.UsuarioService;
import com.foletto.desafioApiTransferencia.services.TransferenciaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("test")
public class TestConfig implements CommandLineRunner {

    @Autowired
    TransferenciaService transferenciaService;

    @Autowired
    UsuarioService usuarioService;

    /**
     * Popula DB ao iniciar aplicação.
     *
     * @param args: String
     * @throws Exception
     */
    @Override
    public void run(String... args) throws Exception {
        int contaUsuario01 = 1000;
        int contaUsuario02 = 2000;
        int contaUsuario03 = 3000;

        Usuario usuario01 = new Usuario("Usuario 01", contaUsuario01, 2000);
        Usuario usuario02 = new Usuario("Usuario 02", contaUsuario02, 3000);
        Usuario usuario03 = new Usuario("Usuario 03", contaUsuario03, 500);
        usuarioService.create(usuario01);
        usuarioService.create(usuario02);
        usuarioService.create(usuario03);

        Transferencia transferencia01 = new Transferencia(contaUsuario01, contaUsuario02, 500);
        Transferencia transferencia02 = new Transferencia(contaUsuario01, contaUsuario02, 1500);
        Transferencia transferencia03 = new Transferencia(contaUsuario02, contaUsuario03, 50);
        Transferencia transferencia04 = new Transferencia(contaUsuario02, contaUsuario01, 300);
        Transferencia transferencia05 = new Transferencia(contaUsuario03, contaUsuario02, 100);
        Transferencia transferencia06 = new Transferencia(contaUsuario03, contaUsuario01, 700);
        transferenciaService.create(transferencia01);
        transferenciaService.create(transferencia02);
        transferenciaService.create(transferencia03);
        transferenciaService.create(transferencia04);
        transferenciaService.create(transferencia05);
        transferenciaService.create(transferencia06);
    }
}
