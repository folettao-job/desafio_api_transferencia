package com.foletto.desafioApiTransferencia.resources.exceptions;

import com.foletto.desafioApiTransferencia.services.exception.DatabaseException;
import com.foletto.desafioApiTransferencia.services.exception.ResourceNotFoundException;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.Instant;

public class ResourceExceptionHandler {

    /**
     * Retorna resposta de erro quando não encontrado o recurso
     *
     * @param e: ResourceNotFoundException
     * @param request: HttpServletRequest
     * @return ResponseEntity<StandardError>
     */
    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<StandardError> resourceNotFound(ResourceNotFoundException e, HttpServletRequest request) {
        String error = "Resource not found";
        HttpStatus status = HttpStatus.NOT_FOUND;
        StandardError err = new StandardError(Instant.now(), status.value(), error, e.getMessage(), request.getRequestURI());
        return ResponseEntity.status(status).body(err);
    }

    /**
     * Retorna resposta para problemas relacionados ao banco de dados
     *
     * @param e: DatabaseException
     * @param request: HttpServletRequest
     * @return ResponseEntity<StandardError>
     */
    @ExceptionHandler(DatabaseException.class)
    public ResponseEntity<StandardError> database(DatabaseException e, HttpServletRequest request) {
        String error = "Database error";
        HttpStatus status = HttpStatus.BAD_REQUEST;
        StandardError err = new StandardError(Instant.now(), status.value(), error, e.getMessage(), request.getRequestURI());
        return ResponseEntity.status(status).body(err);
    }
}
