package com.foletto.desafioApiTransferencia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DesafioApiTransferenciaApplication {

	public static void main(String[] args) {
		SpringApplication.run(DesafioApiTransferenciaApplication.class, args);
	}

}
