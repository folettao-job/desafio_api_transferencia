package com.foletto.desafioApiTransferencia.controllers;

import com.foletto.desafioApiTransferencia.entities.Transferencia;
import com.foletto.desafioApiTransferencia.services.TransferenciaService;
import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/usuario/transferencia")
public class TransferenciaController {
    private final TransferenciaService transferenciaService;

    /**
     * Construtor
     *
     * @param transferenciaService: TransferenciaService()
     */
    public TransferenciaController(TransferenciaService transferenciaService) {
        this.transferenciaService = transferenciaService;
    }

    /**
     * Criação da transferência
     *
     * @param transferencia: Transferencia()
     * @return Transferencia()
     */
    @PostMapping
    public Transferencia create(@RequestBody @Valid Transferencia transferencia) {
        return transferenciaService.create(transferencia);
    }

    /**
     * Lista completa das transferências realizadas por uma determinada conta
     *
     * @param conta: int
     * @return Optional<List<Transferencia>>
     */
    @GetMapping(value = "{conta}")
    public Optional<List<Transferencia>> listAllByConta(@PathVariable int conta) {
        var list = transferenciaService.listAllByUser(conta);
        if (list.isEmpty())
            return null;
        return list;
    }
}
