package com.foletto.desafioApiTransferencia.controllers;

import com.foletto.desafioApiTransferencia.entities.Usuario;
import com.foletto.desafioApiTransferencia.services.UsuarioService;
import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/usuario")
public class UsuarioController {
    private final UsuarioService usuarioService;

    public UsuarioController(UsuarioService userService) {
        this.usuarioService = userService;
    }

    /**
     * Recebe requisição via http
     *
     * @param usuario: Usuario()
     * @return List<Usuario>
     */
    @PostMapping
    public List<Usuario> create(@RequestBody @Valid Usuario usuario) {
        return usuarioService.create(usuario);
    }

    /**
     * Lista todos usuários cadastrados
     *
     * @return List<Usuario>
     */
    @GetMapping
    public List<Usuario> list() {
        return usuarioService.listAll();
    }

    /**
     * Lista usuários através do código da conta
     *
     * @param conta: int
     * @return Optional<Usuario>
     */
    @GetMapping(value = "{conta}")
    public Optional<Usuario> listByConta(@PathVariable int conta) {
        return usuarioService.listByConta(conta);
    }

    /**
     * Realiza update do usuário (padrão create())
     *
     * @param id: Long
     * @param usuario: Usuario()
     * @return Usuario()
     */
    @PutMapping("{id}")
    public Usuario update(@PathVariable Long id, @RequestBody @Valid Usuario usuario) {
        return usuarioService.update(id, usuario);
    }

    /**
     * Realiza deleção de usuário
     *
     * @param id: Long
     * @return List<Usuario>
     */
    @DeleteMapping("{id}")
    public List<Usuario> delete(@PathVariable Long id) {
        return usuarioService.delete(id);
    }
}
