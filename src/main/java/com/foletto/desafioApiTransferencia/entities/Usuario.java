package com.foletto.desafioApiTransferencia.entities;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.Size;

import java.io.Serializable;

@Entity
@Table(name = "tb_usuarios")
public class Usuario implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotBlank
    @Size(min = 2, message = "Nome deve ser maior que 2 caracteres.")
    private String nome;
    @NotNull
    @Positive
    @Column(unique = true)
    private int conta;
    @NotNull
    @Positive
    private int saldo;

    /**
     * Construtor
     */
    public Usuario() {
    }

    /**
     * Construtor
     *
     * @param usuario: Usuario()
     */
    public Usuario(Usuario usuario) {
        this.nome = usuario.getNome();
        this.conta = usuario.getConta();
        this.saldo = usuario.getSaldo();
    }

    /**
     * Construtor
     *
     * @param nome: String
     * @param conta: int
     * @param saldo: int
     */
    public Usuario(String nome, int conta, int saldo) {
        this.nome = nome;
        this.conta = conta;
        this.setSaldo(saldo);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getConta() {
        return conta;
    }

    public void setConta(int conta) {
        this.conta = conta;
    }

    public int getSaldo() {
        return saldo / 100;
    }

    public void setSaldo(int saldo) {
        this.saldo = saldo * 100;
    }
}
