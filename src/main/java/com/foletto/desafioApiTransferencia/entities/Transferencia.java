package com.foletto.desafioApiTransferencia.entities;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;

import java.io.Serializable;
import java.time.Instant;

@Entity
@Table(name = "tb_transferencia")
public class Transferencia implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull
    @Positive
    private int origem;
    @NotNull
    @Positive
    private int destino;
    @NotNull
    @Positive
    private int valor;
    @Column(columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Instant moment = Instant.now();
    private int status;
    private String motivo;

    /**
     * Construtor
     */
    public Transferencia() {
    }

    /**
     * Construtor
     *
     * @param transferencia: Transferencia()
     */
    public Transferencia(Transferencia transferencia) {
        this.origem = transferencia.getOrigem();
        this.destino = transferencia.getDestino();
        this.valor = transferencia.getValor();
        this.moment = transferencia.getMoment();
        this.status = transferencia.getStatus();
        this.motivo = transferencia.getMotivo();
    }

    /**
     * Construtor
     *
     * @param origem: int
     * @param destino: int
     * @param valor: int
     */
    public Transferencia(int origem, int destino, int valor) {
        this.origem = origem;
        this.destino = destino;
        this.setValor(valor);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getOrigem() {
        return origem;
    }

    public void setOrigem(int origem) {
        this.origem = origem;
    }

    public int getDestino() {
        return destino;
    }

    public void setDestino(int destino) {
        this.destino = destino;
    }

    public int getValor() {
        return valor / 100;
    }

    public void setValor(int valor) {
        this.valor = valor * 100;
    }

    public Instant getMoment() {
        return moment;
    }

    public void setMoment(Instant moment) {
        this.moment = moment;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }
}
