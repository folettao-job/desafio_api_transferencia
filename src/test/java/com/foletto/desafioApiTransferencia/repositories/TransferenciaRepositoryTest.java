package com.foletto.desafioApiTransferencia.repositories;

import com.foletto.desafioApiTransferencia.entities.Transferencia;
import com.foletto.desafioApiTransferencia.entities.Usuario;
import jakarta.persistence.EntityManager;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@ActiveProfiles("test")
class TransferenciaRepositoryTest {

    @Autowired
    TransferenciaRepository transferenciaRepositor;

    @Autowired
    EntityManager entityManager;

    @Test
    @DisplayName("Retorno sucesso transferência DB")
    void listAllByUserSuccess() {
        int contaUsuario01 = 10;
        int contaUsuario02 = 20;

        this.createUsuario(new Usuario("Usuario 01", contaUsuario01, 1000));
        this.createUsuario(new Usuario("Usuario 01", contaUsuario02, 1000));

        Transferencia transferencia = new Transferencia(contaUsuario01, contaUsuario02, 500);
        this.createTransferencia(transferencia);

        Optional<List<Transferencia>> result = this.transferenciaRepositor.listAllByUser(contaUsuario01);

        assertThat(result.isPresent()).isTrue();
    }

    @Test
    @DisplayName("Retorno falha transferência DB")
    void listAllByUserFailure() {
        int contaUsuario01 = 10;
        int contaUsuario02 = 20;

        Transferencia transferencia = new Transferencia(contaUsuario01, contaUsuario02, 500);
        this.createTransferencia(transferencia);

        Optional<List<Transferencia>> result = this.transferenciaRepositor.listAllByUser(contaUsuario01);

        assertThat(result.isPresent()).isTrue();
        assertThat(result.get().get(0).getStatus() == 0).isTrue();
    }

    private Transferencia createTransferencia(Transferencia transferencia) {
        Transferencia result = new Transferencia(transferencia);
        this.entityManager.persist(result);
        return result;
    }

    private Usuario createUsuario(Usuario usuario) {
        Usuario result = new Usuario(usuario);
        this.entityManager.persist(result);
        return result;
    }
}