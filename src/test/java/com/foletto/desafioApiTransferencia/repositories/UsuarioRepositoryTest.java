package com.foletto.desafioApiTransferencia.repositories;

import com.foletto.desafioApiTransferencia.entities.Usuario;
import jakarta.persistence.EntityManager;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@ActiveProfiles("test")
class UsuarioRepositoryTest {

    @Autowired
    UsuarioRepository usuarioRepository;

    @Autowired
    EntityManager entityManager;

    @Test
    @DisplayName("Retorno com sucesso usuário DB")
    void searchByContaSuccess() {
        int conta = 10;
        Usuario usuario = new Usuario("Usuario 01", conta, 1000);
        this.createUsuario(usuario);

        Optional<Usuario> result = this.usuarioRepository.searchByConta(conta);

        assertThat(result.isPresent()).isTrue();
    }

    @Test
    @DisplayName("Retorno com falha usuário DB")
    void searchByContaFailure() {
        int conta = 10;

        Optional<Usuario> result = this.usuarioRepository.searchByConta(conta);

        assertThat(result.isEmpty()).isTrue();
    }

    private Usuario createUsuario(Usuario usuario) {
        Usuario result = new Usuario(usuario);
        this.entityManager.persist(result);
        return result;
    }
}