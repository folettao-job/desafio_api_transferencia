package com.foletto.desafioApiTransferencia.controllers;

import com.foletto.desafioApiTransferencia.entities.Transferencia;
import com.foletto.desafioApiTransferencia.entities.Usuario;
import com.foletto.desafioApiTransferencia.services.TransferenciaService;
import com.foletto.desafioApiTransferencia.services.UsuarioService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;

import static java.lang.String.format;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TransferenciaControllerIT {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    UsuarioService usuarioService;

    @Autowired
    TransferenciaService transferenciaService;

    private void createUsuario (int contaUsuario01, int contaUsuario02) {
        Usuario usuario = new Usuario("Usuario IT", contaUsuario01, 600);
        usuarioService.create(usuario);
        usuario = new Usuario("Usuario IT 2", contaUsuario02, 1000);
        usuarioService.create(usuario);
    }

    @Test
    @DisplayName("Transferência realizada com sucesso")
    public void createTransferenciaSuccess() {
        int contaUsuario01 = 998;
        int contaUsuario02 = 999;

        this.createUsuario(contaUsuario01, contaUsuario02);

        Transferencia transferencia = new Transferencia(contaUsuario01, contaUsuario02, 500);

        Transferencia result = restTemplate.postForObject(
                "/api/usuario/transferencia",
                transferencia,
                Transferencia.class
        );

        assertNotNull(result);
        assertEquals(1, result.getStatus());
    }

    @Test
    @DisplayName("Transferência realizada com falha: Saldo insuficiente")
    public void createTransferenciaSaldoInsuficienteFailure() {
        int contaUsuario01 = 996;
        int contaUsuario02 = 997;

        this.createUsuario(contaUsuario01, contaUsuario02);

        Transferencia transferencia = new Transferencia(contaUsuario01, contaUsuario02, 700);

        Transferencia result = restTemplate.postForObject(
                "/api/usuario/transferencia",
                transferencia,
                Transferencia.class
        );

        assertNotNull(result);
        assertEquals(0, result.getStatus());
        assertNotNull(result.getMotivo());
    }

    @Test
    @DisplayName("Transferência realizada com falha: Limite maior que R$ 1000,00")
    public void createTransferenciaLimiteAtingidoFailure() {
        int contaUsuario01 = 994;
        int contaUsuario02 = 995;

        this.createUsuario(contaUsuario01, contaUsuario02);

        Transferencia transferencia = new Transferencia(contaUsuario01, contaUsuario02, 1100);

        Transferencia result = restTemplate.postForObject(
                "/api/usuario/transferencia",
                transferencia,
                Transferencia.class
        );

        assertNotNull(result);
        assertEquals(0, result.getStatus());
        assertNotNull(result.getMotivo());
    }

    @Test
    @DisplayName("Transferência: Retorno com suscesso da lista por conta")
    public void createTransferenciaListaPorContaSuccess() {
        int contaUsuario01 = 990;
        int contaUsuario02 = 991;

        this.createUsuario(contaUsuario01, contaUsuario02);

        transferenciaService.create(new Transferencia(contaUsuario01, contaUsuario02, 100));

        Transferencia[] result = restTemplate.getForObject(
                format("/api/usuario/transferencia/%s", contaUsuario01),
                Transferencia[].class
        );

        assertNotNull(result);
        assertEquals(1, result.length);
        assertNull(result[0].getMotivo());
    }

    @Test
    @DisplayName("Transferência: Retorno com falha da lista por conta")
    public void createTransferenciaListaPorContaFailure() {
        int contaUsuario01 = 992;
        int contaUsuario02 = 993;

        this.createUsuario(contaUsuario01, contaUsuario02);

        transferenciaService.create(new Transferencia(contaUsuario01, contaUsuario02, 100));

        Transferencia[] result = restTemplate.getForObject(
                format("/api/usuario/transferencia/%s", contaUsuario02),
                Transferencia[].class
        );

        assertNotNull(result);
        assertEquals(0, result.length);
    }
}
