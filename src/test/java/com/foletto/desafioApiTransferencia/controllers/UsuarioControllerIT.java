package com.foletto.desafioApiTransferencia.controllers;

import com.foletto.desafioApiTransferencia.entities.Usuario;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.web.client.RestClientException;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UsuarioControllerIT {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    @DisplayName("Criação usuário sucesso")
    public void createUsuarioSuccess() {
        Usuario usuario = new Usuario("Usuario IT", 999999, 2000);

        Usuario[] usuarios = restTemplate.postForObject("/api/usuario", usuario, Usuario[].class);

        assertNotNull(usuarios);
    }

    @Test
    @DisplayName("Criação usuário falha - saldo negativo")
    public void createUsuarioSaldoNegativoFailure() {
        try {
            Usuario usuario = new Usuario("Usuario IT", 9999999, -2000);

            Usuario[] usuarios = restTemplate.postForObject("/api/usuario", usuario, Usuario[].class);
        } catch (RestClientException error) {
            return;
        }
    }

    @Test
    @DisplayName("Criação usuário falha - conta duplicada")
    public void createUsuarioContaDuplicadaFailure() {
        try {
            Usuario usuario = new Usuario("Usuario IT", 9999999, 2000);
            Usuario[] usuarios01 = restTemplate.postForObject("/api/usuario", usuario, Usuario[].class);

            usuario = new Usuario("Usuario IT 2", 9999999, 1000);
            Usuario[] usuarios02 = restTemplate.postForObject("/api/usuario", usuario, Usuario[].class);
        } catch (RestClientException error) {
            return;
        }
    }

}
